# Pixels

An app to let users search for images by tag, and share them with their contacts. This app uses Flickr’s open source API for fetching the images.

# App Components:

**MainActivity** – A container for the different fragments used.

**SearchFragment** – Uses a _**Viewpager**_ to display a slideshow of some popular photos on Flickr

**ResultsFragment** – Uses _**GridView**_ to display all the search results in a 2 column grid

**ImageFragment** – Full screen view of the selected image and lets the user share the image.

The design decision to use fragments instead of activities comes from the fact that this is a simple app and using fragments with a single activity would make communication between the fragments easier and faster. Also, it is much easier to handle the lifecycle events of fragments within a single activity.


# Libraries used:

**Retrofit2** – For all network communications as this library by Square handles all the network calls in background thread automatically.

**Jackson Converter** – This goes with retrofit very well and is one of the standard converters used to convert JSON data to POJO

**Picasso** – Picasso by square is another useful library to load all the images, even remote images, and automatically stores the images in cache for as long as the app is running.

**PinchToZoom** – A library on GitHub used to provide pinch to zoom functionality for the images displayed in ImageFragment. (Ref: https://github.com/martinwithaar/PinchToZoom)

**_Note:_** I didn’t research the above mentioned library - PinchToZoom very well, and so have no details on the way it is implementing the said functionality and what the affects of this on device’s performance are.