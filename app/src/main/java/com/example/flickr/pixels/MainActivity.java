package com.example.flickr.pixels;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.flickr.pixels.api.photos.model.Photo;

public class MainActivity extends AppCompatActivity
        implements SearchFragment.OnSearchListener, ResultsFragment.OnClickListener{

    public static final String SEARCH_TAG = "searchTag";
    public static final String SELECTED_PHOTO = "selectedPhoto";

    private SearchFragment mSearchFragment;
    private ResultsFragment mResultsFragment;
    private ImageFragment mImageFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // If the activity is being recreated due to configuration change
        // no need to instantiate a new SearchFragment.
        if (savedInstanceState == null) {
            mSearchFragment = new SearchFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, mSearchFragment, SearchFragment.TAG)
                    .commit();
        }
    }

    /**
     * Capture the user input (tag) and pass it to {@link ResultsFragment}
     * to get the search results for the tag.
     *
     * @param textInput
     */
    @Override
    public void onUserSearch(String textInput) {
        mResultsFragment = new ResultsFragment();
        Bundle args = new Bundle();
        args.putString(SEARCH_TAG, textInput);
        mResultsFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, mResultsFragment, ResultsFragment.TAG)
                .addToBackStack(ResultsFragment.TAG)
                .commit();
    }

    /**
     * Capture the user click on an image in search results within {@link ResultsFragment}
     * and pass the photo url to {@link ImageFragment} to show the image in full screen.
     *
     * @param photo
     */
    @Override
    public void onImageClick(Photo photo) {
        mImageFragment = new ImageFragment();
        String url = String.format(getString(R.string.image_url_builder),
                photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret());
        Bundle args = new Bundle();
        args.putString(SELECTED_PHOTO, url);
        mImageFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, mImageFragment, ImageFragment.TAG)
                .addToBackStack(ImageFragment.TAG)
                .commit();
    }
}
