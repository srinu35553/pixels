package com.example.flickr.pixels;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.example.flickr.pixels.adapter.WelcomePagerAdapter;
import com.example.flickr.pixels.api.Access;
import com.example.flickr.pixels.api.photos.FlickrPhotosApi;
import com.example.flickr.pixels.api.photos.model.FlickrPhotosResponse;
import com.example.flickr.pixels.api.photos.model.Photo;
import com.example.flickr.pixels.util.FadePagerTransition;
import com.example.flickr.pixels.util.SlowScroller;

import java.lang.reflect.Field;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A Search Fragment to take the user input for the tsearch tag.
 * <p>
 * Activities that contain this fragment must implement the
 * {@link OnSearchListener} interface
 * to handle interaction events.
 */
public class SearchFragment extends Fragment implements TextView.OnEditorActionListener{

    public static final String TAG = SearchFragment.class.getSimpleName();
    private static final String INTERESTING_PHOTOS = "interestingness";

    private OnSearchListener mListener;
    private WelcomePagerAdapter mWelcomePagerAdapter;
    private ViewPager mWelcomeViewPager;
    private EditText mSearchInput;
    private List<Photo> mPhotos;
    private Handler mHandler;
    private Runnable mUpdate;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSearchListener) {
            mListener = (OnSearchListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSearchListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Save the instance state on configuration changes
        this.setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        mSearchInput = rootView.findViewById(R.id.search_input);
        mSearchInput.setOnEditorActionListener(this);

        // Using a ViewPager to show some photos as a slideshow on this fragment.
        // This idea is inspired from the Flickr home page... https://www.flickr.com/
        mWelcomeViewPager = rootView.findViewById(R.id.welcome_image_pager);
        mWelcomePagerAdapter = new WelcomePagerAdapter(getActivity());
        mWelcomeViewPager.setAdapter(mWelcomePagerAdapter);
        // PageTransformer which sets the animation for page change to a fade rather than translate
        mWelcomeViewPager.setPageTransformer(false, new FadePagerTransition());
        setSlowScroller(mWelcomeViewPager);

        // Determine if photos already exist (handling cases such as configuration change)
        if(mPhotos != null) {
            Log.d(TAG, "Re-using the photos, skipping API call");
            mWelcomePagerAdapter.setPhotos(mPhotos);
            mWelcomePagerAdapter.notifyDataSetChanged();
        } else {
            Log.d(TAG, "Fetching new photos");
            getWelcomePhotos();
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        startSlideShow();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopSlideShow();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Remove the listener when fragment gets detached
        mListener = null;
    }

    /* Slow the animation duration for the given viewpager using Java reflections */
    private void setSlowScroller(ViewPager viewPager) {
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            SlowScroller scroller = new SlowScroller(viewPager.getContext());
            mScroller.set(viewPager, scroller);
        } catch (Exception e) {
            Log.e(TAG, "Error updating the scroll duration using reflections");
            e.printStackTrace();
        }
    }

    /* Use Handler to update the page every 7.5 seconds */
    private void startSlideShow() {
        Log.d(TAG, "Starting the slideshow");
        mHandler = new Handler();
        mUpdate = new Runnable() {
            public void run() {
                if(mWelcomePagerAdapter.getCount() > 0) {
                    int currentPage;
                    // Check if we reach the end and start over
                    if (mWelcomeViewPager.getCurrentItem() == mWelcomePagerAdapter.getCount() - 1) {
                        currentPage = 0;
                    } else {
                        currentPage = mWelcomeViewPager.getCurrentItem() + 1;
                    }
                    mWelcomeViewPager.setCurrentItem(currentPage, true);
                }
                mHandler.postDelayed(mUpdate, 7500);
            }
        };
        mHandler.postDelayed(mUpdate, 7500);
    }

    /* Remove any callbacks from handler, which stops the slide show */
    private void stopSlideShow() {
        Log.d(TAG, "Stopping the slideshow");
        mHandler.removeCallbacks(mUpdate);
    }

    /* Use the Flickr API to fetch the "interestingness" images*/
    private void getWelcomePhotos() {
        FlickrPhotosApi api = Access.getInstance().getFlickrPhotosApi();
        Call<FlickrPhotosResponse> welcomeImagesRequest = api.getInterestingImages();
        welcomeImagesRequest.enqueue(new Callback<FlickrPhotosResponse>() {
            @Override
            public void onResponse(Call<FlickrPhotosResponse> call,
                                   Response<FlickrPhotosResponse> response) {
                if (response.isSuccessful()) {
                    FlickrPhotosResponse photosResponse = response.body();
                    if (photosResponse != null && photosResponse.getPhotos() != null) {
                        // Get the first 10 photos from the response to use in the slideshow
                        // and update the view pager adapter
                        mPhotos = photosResponse.getPhotos().getList().subList(0, 10);
                        mWelcomePagerAdapter.setPhotos(mPhotos);
                        mWelcomePagerAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<FlickrPhotosResponse> call, Throwable t) {
                Log.d(TAG, "Fetching interesting images failed");
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        // Handle the user click on search button on soft keyboard or
        // enter key on any hard keyboard attached
        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                (event.getKeyCode() == KeyEvent.KEYCODE_ENTER &&
                event.getAction() == KeyEvent.ACTION_DOWN)) {
            String searchInput = mSearchInput.getText().toString();
            if(!searchInput.isEmpty()) {
                // Use the listener to send the user input to the MainActivity
                // Note: Not doing any input validation as suggested in the requirements
                mListener.onUserSearch(searchInput);
            }
            return true;
        }
        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnSearchListener {
        void onUserSearch(String textInput);
    }
}
