package com.example.flickr.pixels.util;

import android.support.v4.content.FileProvider;

/**
 * Generic file provider used as an interface for other apps to access the app's files.
 */
public class GenericFileProvider extends FileProvider{
}
