package com.example.flickr.pixels;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;

import com.example.flickr.pixels.adapter.ResultsGridAdapter;
import com.example.flickr.pixels.api.Access;
import com.example.flickr.pixels.api.photos.FlickrPhotosApi;
import com.example.flickr.pixels.api.photos.model.FlickrPhotosResponse;
import com.example.flickr.pixels.api.photos.model.Photo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Fragment used to display the results for a tag search
 */
public class ResultsFragment extends Fragment implements TextWatcher, GridView.OnItemClickListener{

    public static final String TAG = ResultsFragment.class.getSimpleName();

    private OnClickListener mListener;
    private EditText mResultsFilter;
    private GridView mResultsGrid;
    private String mSearchTag;
    private ResultsGridAdapter mGridAdapter;
    private List<Photo> mPhotos;

    public ResultsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClickListener) {
            mListener = (OnClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnClickListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args != null) {
            // Get the tag input by the user
            mSearchTag = args.getString(MainActivity.SEARCH_TAG);
        }
        // Save the instance state on configuration changes
        this.setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_results, container, false);

        mResultsFilter = rootView.findViewById(R.id.results_filter);
        mResultsFilter.addTextChangedListener(this);

        // Using a grid view with fixed height for grid view item to display the search results
        mResultsGrid = rootView.findViewById(R.id.search_result_grid);
        mGridAdapter = new ResultsGridAdapter(getActivity());
        mResultsGrid.setAdapter(mGridAdapter);
        mResultsGrid.setOnItemClickListener(this);

        // Determine if photos already exist (handling cases such as configuration change)
        if (mPhotos != null) {
            Log.d(TAG, "Re-using the photos, skipping API call");
            mGridAdapter.setPhotos(mPhotos);
            mGridAdapter.notifyDataSetChanged();
        } else {
            Log.d(TAG, "Fetching new photos");
            getSearchResults();
        }

        return rootView;
    }

    /* Use the Flickr API to get the search results using the tag provided*/
    private void getSearchResults() {
        FlickrPhotosApi api = Access.getInstance().getFlickrPhotosApi();
        Call<FlickrPhotosResponse> searchRequest = api.getRecentImages(mSearchTag);
        searchRequest.enqueue(new Callback<FlickrPhotosResponse>() {
            @Override
            public void onResponse(Call<FlickrPhotosResponse> call,
                                   Response<FlickrPhotosResponse> response) {
                if (response.isSuccessful()) {
                    FlickrPhotosResponse photosResponse = response.body();
                    if (photosResponse != null && photosResponse.getPhotos() != null) {
                        // Fetch the photos and update the grid view adapter
                        mPhotos = photosResponse.getPhotos().getList();
                        mGridAdapter.setPhotos(mPhotos);
                        mGridAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<FlickrPhotosResponse> call, Throwable t) {
                Log.d(TAG, "Fetching interesting images failed");
                t.printStackTrace();
            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // No need to handle this case
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(mGridAdapter != null) {
            // Use the filter defined within ResultsGridAdapter to filter the results displayed
            mGridAdapter.getFilter().filter(s);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        // No need to handle this case
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(mListener != null && mGridAdapter.getFilteredPhotos().size() >= position) {
            // Use the listener to notify the MainActivity on user click on any image
            mListener.onImageClick(mGridAdapter.getFilteredPhotos().get(position));
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnClickListener {
        void onImageClick(Photo photo);
    }
}
