package com.example.flickr.pixels.api;

import com.example.flickr.pixels.api.photos.FlickrPhotosApi;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class Access {

    private static Access sInstance;

    // Timeout for all API calls
    private static final int TIMEOUT = 60;
    // Base URL for the Flickr APIs
    private static final String BASE_URL = "https://api.flickr.com";

    private OkHttpClient.Builder mOkClientBuilder;
    private JacksonConverterFactory mConverter;

    private FlickrPhotosApi mFlickrPhotosApi;

    /**
     * For thread-safe approach, double checking singleton sInstance.
     *
     * The approach on double-checking is debatable, but I think this provides a more robust
     * and safe singleton instance over the other approaches.
     */
    public static Access getInstance() {
        if (sInstance == null) {
            synchronized (Access.class) {
                // Double check
                if (sInstance == null) {
                    sInstance = new Access();
                }
            }
        }
        return sInstance;
    }

    private Access() {
        // Prepare HTTP client builder
        mOkClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .followRedirects(false);

        // Prepare Jackson Converter factory
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mConverter = JacksonConverterFactory.create(mapper);
    }

    private Retrofit getRetrofit(int interceptorFlags, String baseUrl) {
        // Create a new Retrofit builder
        Retrofit.Builder builder = new Retrofit.Builder();

        // Return if the baseUrl is null
        if (baseUrl == null) return null;
        // Set the API base url to retrofit
        builder.baseUrl(baseUrl);

        // Add custom interceptor to OkHttpClient, if any
        if (interceptorFlags != 0) {
            mOkClientBuilder.addInterceptor(new Interceptor(interceptorFlags));
        }

        // Add logging interceptor to OkHttpClient
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        mOkClientBuilder.addInterceptor(loggingInterceptor);

        return builder.client(mOkClientBuilder.build())
                .addConverterFactory(mConverter)
                .build();
    }

    public FlickrPhotosApi getFlickrPhotosApi() {
        if (mFlickrPhotosApi != null) return mFlickrPhotosApi;

        // Get the Retrofit object and create the API interface
        Retrofit retrofit = getRetrofit(Flags.API_KEY, BASE_URL);
        if (retrofit != null) {
            mFlickrPhotosApi = retrofit.create(FlickrPhotosApi.class);
        }
        return mFlickrPhotosApi;
    }

    private static class Flags {
        // Flag to add Flickr api key as query parameter
        private static final int API_KEY = 0x01;
    }

    private class Interceptor implements okhttp3.Interceptor {
        private int flags;

        Interceptor(int flags) {
            this.flags = flags;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            // New request builder to be used to update headers, url, etc.
            Request.Builder newRequestBuilder = originalRequest.newBuilder();
            // New Http url builder to be used to add query params, path params, etc.
            HttpUrl.Builder newUrlBuilder = originalRequest.url().newBuilder();

            if ((flags & Flags.API_KEY) != 0) {
                // add api_key query param to the new Http Url
                newUrlBuilder.addQueryParameter("api_key",
                        "6a8e0794c29c9c6641e639af0f2e0754");
            }

            // Build the new URL and set to new request
            newRequestBuilder.url(newUrlBuilder.build());
            // proceed with the new request
            return chain.proceed(newRequestBuilder.build());
        }
    }
}
