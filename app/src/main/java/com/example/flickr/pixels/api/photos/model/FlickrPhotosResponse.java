package com.example.flickr.pixels.api.photos.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FlickrPhotosResponse {

    @JsonProperty("photos")
    private Photos photos;
    @JsonProperty("stat")
    private String stat;

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}
