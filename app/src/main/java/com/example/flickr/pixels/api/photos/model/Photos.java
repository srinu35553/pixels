package com.example.flickr.pixels.api.photos.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Photos {

    @JsonProperty("page")
    private int page;
    @JsonProperty("pages")
    private int pages;
    @JsonProperty("perpage")
    private int perpage;
    @JsonProperty("total")
    private String total;
    @JsonProperty("photo")
    private List<Photo> list = null;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPerpage() {
        return perpage;
    }

    public void setPerpage(int perpage) {
        this.perpage = perpage;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<Photo> getList() {
        return list;
    }

    public void setList(List<Photo> list) {
        this.list = list;
    }
}
