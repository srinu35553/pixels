package com.example.flickr.pixels.util;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

public class SlowScroller extends Scroller {

    // Setting the scroller duration to 5 seconds
    private int mScrollDuration = 5000;

    public SlowScroller(Context context) {
        super(context);
    }

    public SlowScroller(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, mScrollDuration);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy) {
        super.startScroll(startX, startY, dx, dy, mScrollDuration);
    }
}