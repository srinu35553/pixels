package com.example.flickr.pixels.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.flickr.pixels.R;
import com.example.flickr.pixels.api.photos.model.Photo;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class ResultsGridAdapter extends BaseAdapter implements Filterable{

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<Photo> mPhotos;
    private List<Photo> mFilteredPhotos;

    public ResultsGridAdapter(Context context) {
        mContext = context;
        mPhotos = new ArrayList<>();
        mFilteredPhotos = new ArrayList<>();
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setPhotos(List<Photo> photos) {
        if (photos != null) {
            // Maintain an original photos list in mPhotos and
            // filtered photos list in mFilteredPhotos
            mPhotos.clear();
            mPhotos.addAll(photos);

            mFilteredPhotos.clear();
            mFilteredPhotos.addAll(photos);
        }
    }

    public List<Photo> getFilteredPhotos() {
        return new ArrayList<>(mFilteredPhotos);
    }

    @Override
    public int getCount() {
        return mFilteredPhotos.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilteredPhotos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        GridViewHolder gridViewHolder;

        if (itemView == null) {
            itemView = mLayoutInflater.inflate(R.layout.grid_vew_item, parent, false);
            gridViewHolder = new GridViewHolder(itemView);
            itemView.setTag(gridViewHolder);
        } else {
            gridViewHolder = (GridViewHolder) convertView.getTag();
        }

        String url = null;
        Photo photo = mFilteredPhotos.get(position);
        if (photo != null) {
            url = String.format(mContext.getString(R.string.welcome_image_url_builder),
                    photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret());
            // Using Picasso to load the image into imageview
            Picasso.with(mContext).load(url).into(gridViewHolder.gridItemImage);
            gridViewHolder.gridItemTitle.setText(photo.getTitle());
        }

        return itemView;
    }

    @Override
    public Filter getFilter() {
        return new PhotoFilter();
    }

    private class GridViewHolder {
        ImageView gridItemImage;
        TextView gridItemTitle;

        GridViewHolder(View view) {
            gridItemImage = view.findViewById(R.id.grid_item_image);
            gridItemTitle = view.findViewById(R.id.grid_item_title);
        }
    }

    /**
     * PhotoFilter handling the results displayed based on the filter text entered by user
     */
    private class PhotoFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            constraint = constraint.toString().toLowerCase().trim();
            FilterResults results = new FilterResults();

            if (constraint.toString().length() > 0) {
                List<Photo> filteredList = new ArrayList<>();
                for(Photo photo: mPhotos){
                    if(photo.getTitle().toLowerCase().trim().contains(constraint)){
                        filteredList.add(photo);
                    }
                }
                results.values = filteredList;
                results.count = filteredList.size();
            }else {
                results.values = mPhotos;
                results.count = mPhotos.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFilteredPhotos.clear();
            if (results.values != null) {
                mFilteredPhotos.addAll((List<Photo>) results.values);
            }
            notifyDataSetChanged();
        }
    }
}