package com.example.flickr.pixels.api.photos.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Photo {

    @JsonProperty("id")
    private String id;
    @JsonProperty("owner")
    private String owner;
    @JsonProperty("secret")
    private String secret;
    @JsonProperty("server")
    private String server;
    @JsonProperty("farm")
    private int farm;
    @JsonProperty("title")
    private String title;
    @JsonProperty("ispublic")
    private byte ispublic;
    @JsonProperty("isfriend")
    private byte isfriend;
    @JsonProperty("isfamily")
    private byte isfamily;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getFarm() {
        return farm;
    }

    public void setFarm(int farm) {
        this.farm = farm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public byte getIspublic() {
        return ispublic;
    }

    public void setIspublic(byte ispublic) {
        this.ispublic = ispublic;
    }

    public byte getIsfriend() {
        return isfriend;
    }

    public void setIsfriend(byte isfriend) {
        this.isfriend = isfriend;
    }

    public byte getIsfamily() {
        return isfamily;
    }

    public void setIsfamily(byte isfamily) {
        this.isfamily = isfamily;
    }
}
