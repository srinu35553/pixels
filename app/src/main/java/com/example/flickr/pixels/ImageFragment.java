package com.example.flickr.pixels;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Image fragment to display the image full screen and let the user share it.
 */
public class ImageFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = ImageFragment.class.getSimpleName();

    private String mUrl;
    private FloatingActionButton mShareFab;

    public ImageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            // Get the image URL
            mUrl = args.getString(MainActivity.SELECTED_PHOTO);
        }
        // Save instance state on configuration changes
        this.setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_image, container, false);

        ImageView mSelectedImage = rootView.findViewById(R.id.selected_image);
        if (mUrl != null) {
            Picasso.with(getActivity()).load(mUrl).into(mSelectedImage);
        }
        // Using a third party library to handle the pinch to zoom
        // functionality for the image displayed
        // Ref: https://github.com/martinwithaar/PinchToZoom
        mSelectedImage.setOnTouchListener(new ImageMatrixTouchHandler(getActivity()));

        mShareFab = rootView.findViewById(R.id.share_image);
        mShareFab.setOnClickListener(this);

        return rootView;
    }

    /*
     * Download the image and use the ACTION_SEND intent action to show the user a
     * chooser to share the image using any of the supported apps on his/her phone.
     */
    public void shareImage(String url) {
        // Download the image by specifying a target (listener) to
        // Picasso's load method to get the bitmap
        Picasso.with(getActivity()).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent imageShareIntent = new Intent(Intent.ACTION_SEND);
                imageShareIntent.setType("image/jpg");
                imageShareIntent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                // Setting flag to be sure that other apps can use the URI poitnig to app's files
                imageShareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(imageShareIntent, "Share using..."));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Log.e(TAG, "Error downloading the image");
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    /**
     * Save the bitmap as a local file inside app's files
     *
     * @return Uri for the image file
     */
    public Uri getLocalBitmapUri(Bitmap bitmap) {
        Uri bitmapUri = null;
        try {
            File imageFile = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "pixels_" + System.currentTimeMillis() + ".jpg");
            FileOutputStream out = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.close();
            // Using the file provider to get the uri as newer Android versions require
            // the apps to use a file provider rather than a simple uri for other apps to access
            // any files on this app's storage
            bitmapUri = FileProvider.getUriForFile(getActivity(),
                    "com.example.flickr.pixels.provider", imageFile);
        } catch (IOException e) {
            Log.e(TAG, "Error saving the image to a file");
            e.printStackTrace();
        }
        return bitmapUri;
    }

    @Override
    public void onClick(View v) {
        if (v == mShareFab) {
            // Handle click on share fab
            shareImage(mUrl);
        }
    }
}
