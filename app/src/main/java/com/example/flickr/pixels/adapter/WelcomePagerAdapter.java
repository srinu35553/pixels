package com.example.flickr.pixels.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.flickr.pixels.R;
import com.example.flickr.pixels.api.photos.model.Photo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class WelcomePagerAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<Photo> mPhotos = new ArrayList<>();

    public WelcomePagerAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setPhotos(List<Photo> photos) {
        if(photos != null) {
            mPhotos.addAll(photos);
        }
    }

    @Override
    public int getCount() {
        return mPhotos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(
                R.layout.welcome_image_pager_item,
                container, false
        );

        String url = null;
        Photo photo = mPhotos.get(position);
        if (photo != null) {
            url = String.format(mContext.getString(R.string.welcome_image_url_builder),
                    photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret());
        }
        ImageView welcomeImageView = itemView.findViewById(R.id.welcome_image);
        // Use Picasso to load the image into imageview
        Picasso.with(mContext).load(url).into(welcomeImageView);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
