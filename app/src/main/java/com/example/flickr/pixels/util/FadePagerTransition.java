package com.example.flickr.pixels.util;

import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * This class is used to set any ViewPager's page change animation to fade
 * as opposed to the default - translate.
 */
public class FadePagerTransition implements ViewPager.PageTransformer {

    public void transformPage(View view, float position) {

        view.setTranslationX(view.getWidth() * -position);

        if (position <= -1.0F || position >= 1.0F) {
            // position if off to the left or right out of user's view
            view.setAlpha(0.0F);
        } else if (position == 0.0F) {
            // center position
            view.setAlpha(1.0F);
        } else {
            // position is between -1.0F & 1.0F
            view.setAlpha(1.0F - Math.abs(position));
        }
    }
}
