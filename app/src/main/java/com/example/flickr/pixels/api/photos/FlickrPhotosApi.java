package com.example.flickr.pixels.api.photos;

import com.example.flickr.pixels.api.photos.model.FlickrPhotosResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * API Interface defining HTTP CRUD calls
 */
public interface FlickrPhotosApi {

    // GET method for getting the recent images
    // based on the tag input by user
    @GET("/services/rest/?format=json&nojsoncallback=1&method=flickr.photos.search")
    Call<FlickrPhotosResponse> getRecentImages(@Query("tags") String tag);

    // GET method for fetching interesting images
    @GET("/services/rest/?format=json&nojsoncallback=1&method=flickr.interestingness.getList")
    Call<FlickrPhotosResponse> getInterestingImages();
}
